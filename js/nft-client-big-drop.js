// version 1.1
var big = {
    init: (host) => {
        common.setTarget(host);
        $(".c2c-btn").click(common.widgets.copyBtn);
        big.open(big.dropStatus, common.serverError);
    },

    dropStatus: (r) => {
        if (r.open){
            const buy = $("#nftBuyBtn");
            buy.removeClass("inactive");
            buy.click(big.buyNft);

        } else if (r.saleStarts){
            big.saleStarts(r);

        } else if (r.volume){
            $("#nftEventDate").html("We're experiencing high volumes");
            big.quieten();

        } else if (r.presaleStarts){
            const eventDate = $("#nftEventDate")
            const start = common.localDate(r.presaleStarts);
            eventDate.html("Pre-Sale Starts: " + start);
            big.startQueue(r.presaleStarts);

        } else if (r.paymentAddress){
            common.reserve(r);

        } else if (r.presale){
            big.authPreSale(r.presale);

        } else {
            console.error("Unexpected case: response follows.");
            console.error(r);

        }
    },

    saleStarts: (r) => {
        const eventDate = $("#nftEventDate")
        const start = common.localDate(r.saleStarts);
        
        if (r.auth === true){
            const tid = window.sessionStorage.getItem("tid");
            if (tid !==null){
                big.authPreSale(tid);

            } else {
                eventDate.html("This Pre-Sale link has been used... Sale Starts: " + start);
                big.startQueue(r.saleStarts);

            }
        } else {
            eventDate.html("Sale Starts: " + start);
            big.startQueue(r.saleStarts);

        }
    },

    authPreSale: (tid) => {
        $("#nftEventDate").html(
            "Pre-Sale Authorized.  You will have twenty minutes from activating the checkout.");
        $("#nftTimeToEvent").html("");
        const buy = $("#nftBuyBtn");
        buy.removeClass("inactive");
        buy.unbind("click")
        buy.click(big.buyNft);
        window.sessionStorage.setItem("tid", tid);

    },

    startQueue: (t) => {
        const timeToEvent0 = common.timeToEvent(t);
        const tte0 = common.eventTimeToString(timeToEvent0, "Sale Open");
        const buy = $("#nftBuyBtn");
        buy.addClass("inactive");

        $("#nftTimeToEvent").html(tte0);

        var timer = setInterval(()=> {
            var timeToEvent = common.timeToEvent(t);
            const tte = common.eventTimeToString(timeToEvent);
            $("#nftTimeToEvent").html(tte);
    
        }, 1000);

        if (timeToEvent0.hours===0 && timeToEvent0.days===0){
            setTimeout(()=>{
                clearInterval(timer);
                big.open(big.dropStatus, common.serverError);
            }, timeToEvent0.millis + 50); 
        }
    },

    buyNft: () => {
        const tid = window.sessionStorage.getItem("tid");
        $(".waiting").removeClass("hidden");
        big.buy({tid:tid}, (resp) => {
            $(".waiting").addClass("hidden");
            if (resp.saleStarts){
                window.sessionStorage.removeItem("tid");
                big.dropStatus(resp);

            } else if (resp.volume){
                $("#nftEventDate").html("We're experiencing high volumes");
                big.quieten();
            
            } else if (resp.error){
                if (resp.error === "NFT_MAKER_UNAVAILABLE"){
                    $("#nftEventDate").html("Whoops... Minting currently unavailable!  Don't worry, we've got your session and as soon as we're back online, we'll get right back to it");
                    big.quieten();

                } else if (resp.error === "NFT_MAKER_LIMIT"){
                    $("#nftEventDate").html("We're really very busy right now... try again in a few minutes.");
                    big.quieten();

                }
            } else {
                $("#nftEventDate").html("");
                common.widgets.scroll("nftCheckOut");                
                const buy = $("#nftBuyBtn");
                buy.addClass("inactive");
                buy.unbind("click")
                common.reserve(resp);

            }            
        }, common.serverError);

    },

    quieten: () => {
        const buy = $("#nftBuyBtn");
        buy.addClass("inactive");
        buy.unbind("click");
        setTimeout(()=>{
            buy.click(big.buyNft);
            buy.removeClass("inactive");
        }, 10000);

    },
    
    open: (success, error) => {
        var url = window.location.href.split("?");
        url = (url.length==2 ? url[1] : "");
        $.ajax({
            type:"GET",
            url: common.target + "bigdrop/" + url,
            mimeType: common.mime,
            success: success,
            error: error 

        });
    },

    buy: (cmd, success, error) => {
        $.ajax({
            type:"POST",
            url: common.target + "bigdrop",
            data: cmd,
            mimeType: common.mime,
            success: success,
            error: error 

        });
    }
}