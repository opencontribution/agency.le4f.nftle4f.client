const unsub = {
    init: (url) => {
        unsub.setTarget(url);
        unsub.executeUnsubscribe(unsub.unsubscribed);
    },

    unsubscribed: (resp) => {
        const field = $("#unsubscribeEmailAddress");
        if (resp.id){
            field.html("We're sorry to see you go!");

        } else {
            $(".heading-h1").html("Whooopsie daisy!");
            field.html("Something unique and interesting happened, " +   
            "but I'm afraid we didn't register that.  Please email "
            + resp.error + " with 'unsubscribe' in the title");

        }
    },

    executeUnsubscribe: (success, error) => {
        var url = window.location.href.split("?");
        url = (url.length==2 ? url[1] : "");
        $.ajax({
            type:"GET",
            url: unsub.target + "unsubscribe/" + url,
            mimeType: "application/json",
            success: success,
            error: error 

        });
    },

    target : null, 

    setTarget: (target) => {
        if (typeof target === "undefined"){
            throw "target must be set to the target api end point";

        }  if (!target.endsWith("/")){
            target+= "/";

        } if (target.startsWith("http://")){
            console.warn("Not Encrypted. To be used only for testing!");
            unsub.target = target;

        } 
        if (target.startsWith("https://") || target.startsWith("http://")){
            unsub.target = target;

        } else {
            throw "domain must be fully qualified:  i.e. https://nft.le4f.agency/";

        }
        console.log("Target: " + unsub.target);            

    },
}