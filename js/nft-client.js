// v1.1
var app = {    
    init: (target) => {
        app.initNftItem();
        common.setTarget(target);
        app.request.all(app.displayResults, common.serverError);
        app.bindings();

    }, 

    bindings: () => {
        $(".close-pop-up").click(common.widgets.closeBtn);
        $("#success-close").click(common.widgets.closeSuccess);
        $(".c2c-btn").click(common.widgets.copyBtn);

    },

    displayResults: (response) => {
        if (response.error){
            var e = response.error;
            if (e==="NFT_MAKER_UNAVAILABLE"){
                $("#somethingOrOther").removeClass("hidden");

            } else {
                console.log("Unhandled error: " + e);

            }
        } else {
            for (var r in response){
                if (response[r].name!==""){
                    app.addNft(response[r]);

                }
            }    
        }
    },

    addNft: (nft) => {
        var nftItem = app.nftItem;
        app.storeLocalItem(nft);
        nftItem = nftItem.replace("LE4F_NFT_ITEM_ID", nft.id);
        nftItem = nftItem.replace("${POLICY_ID}", nft.policyId);
        nftItem = nftItem.replace("${ASSET_ID}", nft.assetId);
        nftItem = nftItem.replace("${GATEWAY_LINK}", nft.gatewayLink);
        nftItem = nftItem.replace("${TITLE}", nft.name);
        nftItem = nftItem.replace("${PRICE}", nft.priceAda);
        $(".nft-list-wrapper").append(nftItem);
        var nftHtmlItem = $("#" + nft.id);
        nftHtmlItem.click(app.viewNftPopup)
        if (nft.state === "sold"){
            nftHtmlItem.find(".sold").removeClass("hidden");

        } else if (nft.state === "reserved"){
            nftHtmlItem.find(".reserved").removeClass("hidden");

        }                
    },

    storeLocalItem: (nft) => {
        window.sessionStorage.setItem(nft.id, JSON.stringify(nft));

    },

    viewNftPopup: (e) => {
       $(".popup-checkout").removeClass("hidden");
        $(".checkout-step-01").removeClass("hidden");
        $(".checkout-step-02").addClass("hidden");
        const item = $(e.target).closest(".nft-item")
        const nftId = item.attr("id");
        var nft = window.sessionStorage.getItem(nftId);
        nft = JSON.parse(nft);
        $("#artwork-display").attr("src", nft.gatewayLink)
        $("#nft-title-step1").html(nft.name);
        $("#nft-price-step1").html(nft.priceAda);

        const meta = "<p>Policy: " + nft.policyId + 
                     "</p><p>Asset: " + nft.assetId + "</p>";
        $("#specificNftMetaData").html(meta);

        var buy = $("#pay-price");
        buy.unbind("click");
        buy.click(app.purchaseRequest);
        buy.attr("nft-id", nftId);

    },

    purchaseRequest: (e) => {
        const cmd = {
            cmd: "get-address", 
            nftid: $(e.target).attr("nft-id"),
            tokenCount: 1

        }
        $(".waiting").removeClass("hidden");
        common.nft(cmd, common.reserve, common.serverError);

    },

    initNftItem: () => {
        const i = $("#LE4F_NFT_ITEM_ID");
        i.removeClass("hidden");
        app.nftItem = $(".nft-list-wrapper").html();        
        app.nftItem = app.nftItem.replaceAll("\n", "");
        app.nftItem = app.nftItem.replaceAll("\t", "");
        app.nftItem = app.nftItem.replaceAll("  ", "");
        $(".nft-item").remove();

    },

    nftItem: null, 

    request: {
        all: (success, error) => {
            $.ajax({
                type:"GET",
                url: common.target + "all",
                mimeType: common.mime,
                success: success,
                error: error 
            });
        }
    },

}
