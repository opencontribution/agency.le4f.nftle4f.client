// version 1.1
var common = {
    target: null, 
    mime: "application/json",    

    reserve: (response) => {
        $(".waiting").addClass("hidden");
        if (!response.error){
            if (!response.errorCode){
                common.processReserveSuccess(response);

            } else if (response.errorCode===10){
                $("#soldOut").removeClass("hidden");

            }
        } else {
            var e = response.error;
            if (e==="TOKEN_UNAVAILABLE"){
                $(".popup-waiting-que").removeClass("hidden");

            }
        }
    }, 

    processReserveSuccess: (response) => {
        $(".checkout-step-01").addClass("hidden");
        $(".checkout-step-02").removeClass("hidden");            
        $("#nft-address-step2").html(response.paymentAddress);
        $("#nft-price-step2").html(response.adaToSend);
        $("#expires-in").attr("expiry-time", response.expires);
        common.qr($("#address-qr-code"), response.b64QrPng);
        if (response.fixed===true){
            $(".nft-fixed").addClass("hidden");
            $("#nft-pricelist").removeClass("hidden");

        } else {
            common.executeExpiryTimer(response.paymentAddress);

        }
    },

    qr: (imgTagJ, png) => {
        var source = "data:image/png;base64,--qr-base64--";
        source = source.replace("--qr-base64--", png);
        imgTagJ.attr("src", source);

    },

    timeToEvent: (utcString) => {
        if (typeof utcString !== "undefined"){
            const dt = new Date(utcString);
            const t0 = Date.now();
            const ms = dt.getTime() - t0;
            const s = Math.floor(ms / 1000);
            const m = Math.floor(s / 60);
            const h = Math.floor(m / 60);
            const d = Math.floor(h / 24);
            return {
                days: (d), 
                hours: (h%24) , minutes: (m % 60), seconds: (s % 60), 
                milliseconds: (ms % 1000), millis:ms

            }
        }
    },

    eventTimeToString: (timeToEventObj, finalMessage) => {
        const f = {
            resolveMinSec: () => {
                var m = timeToEventObj.minutes;
                var s = timeToEventObj.seconds;
                const mlabel = (m === 1 ? " minute " : " minutes ");
                const slabel = (s === 1 ? " second " : " seconds ");
                return m + mlabel + s + slabel;

            } 
        }
        if (typeof timeToEventObj !== "undefined"){
            if (timeToEventObj.days>0){
                const label = (timeToEventObj.days === 1 ? " day " : " days ");
                const labelh = (timeToEventObj.hours === 1 ? " hour " : " hours ");
                return timeToEventObj.days + label + timeToEventObj.hours + labelh + f.resolveMinSec();

            } else if (timeToEventObj.hours>0){
                const label = (timeToEventObj.hours === 1 ? " hour " : " hours ");
                return timeToEventObj.hours + label + f.resolveMinSec();

            } else if (timeToEventObj.seconds>0 || timeToEventObj.minutes > 0){ 
                return f.resolveMinSec();

            } else {
                if (typeof finalMessage === "undefined"){
                    return "0:00";

                }  else {
                    return finalMessage;

                }
            }
        }
    },

    loop: null,

    executeExpiryTimer: (checkAddress) => {
        const expiryField = $("#expires-in");
        const expiry = expiryField.attr("expiry-time");
        var tte = common.timeToEvent(expiry);
        if (typeof expiry !== "undefined"){
            var ms = 1; 
            common.loop = setInterval(()=>{
                var tte = common.timeToEvent(expiry);
                
                ms = tte.millis;
                const s = Math.floor(ms / 1000);
                const checkForPayment = s % 16 === 0;
                expiryField.html(common.eventTimeToString(tte, "Expired"));

                if (checkForPayment){
                    common.checkForPayment(checkAddress, (e)=>{
                        if (e){
                            clearInterval(common.loop);

                        }
                    });
                }
            }, 1001);
            setTimeout(()=>{
                clearInterval(common.loop);
                $("#timeout").removeClass()

            }, tte.millis);
        }
    },

    checkForPayment: (address, end) => {
        common.nft({cmd:"check-address", address: address},
            (r) => {
                if (typeof r.lovelace === 'number'){
                    if (r.hasToPay === r.lovelace){
                        $(".popup-confirmation").removeClass("hidden");
                        end(true);
    
                    } else if (r.error){
                        console.warn("We're having troubles checking on the payment, right now - busy");
                        console.warn(r.error);
                        end(false);

                    } else if (r.errorCode){
                        console.log("A bridging error occured.  Sorry.");
                        console.log(r);
                        end(true);
                    
                    } else {
                        end(false);
    
                    }
                }
            }, common.serverError);
    },

    nft: (cmd, success, error) => {
        $.ajax({
            type:"POST",
            url: common.target + "all",
            mimeType: common.mime,
            data: cmd, 
            success: success,
            error: error 
        });
    },

    widgets: {
        closeBtn: (e) => {
            $(".popup-checkout").addClass("hidden");
            clearInterval(app.loop);

        },

        closeSuccess: () => {
            $(".popup-confirmation").addClass("hidden");
            $(".popup-checkout").addClass("hidden");

        },

        copyBtn: function(e){
            var $temp = $("<input>");
            var textField = $(e.target
                .parentElement
                .parentElement)
                .find(".copy-target");
            $("body").append($temp);
            const v = $(textField).html();
            console.log(v);
            $temp.val(v).select();
            document.execCommand("copy");
            $temp.remove();
            textField.addClass("nft-notify-copy");
            setTimeout(()=>{
                textField.removeClass("nft-notify-copy");
            }, 200);
    
        },     
        
        scroll: (id) => {
            var field = $("#" + id);
            $('html,body').animate({scrollTop: field.offset().top}, 'slow');
        }
    },

    setTarget: (target) => {
        if (typeof target === "undefined"){
            throw "target must be set to the target api end point";

        }  if (!target.endsWith("/")){
            target+= "/";

        } if (target.startsWith("http://")){
            console.warn("Not Encrypted. To be used only for testing!");
            common.target = target;

        } 
        if (target.startsWith("https://") || target.startsWith("http://")){
            common.target = target;

        } else {
            throw "domain must be fully qualified:  i.e. https://nft.le4f.agency/";

        }
        console.log("Target: " + common.target);            

    },

    localDate: (utcDate) => {
        return new Date(utcDate).toLocaleString(undefined, {
            weekday: "long", 
            day: "2-digit",
            year: "numeric", 
            month: "long",
            hour: "numeric", 
            hour12: true,
            minute: "numeric",
            timeZoneName: "short"       
        });
    },

    serverError: (e) => {
        console.error(e);

    }
}